# Modern Physics 101
Meant as quick reminders this section includes only short notes and formulas.
## Relativity
### Time Dilation
$$
\beta=\frac{v}{c} \\
\tag{Definition of Beta}
\label{eq:relativity_beta}
$$
$$
\Delta t = \frac{\Delta t'}{\sqrt{1-\beta^2}}
\label{eq:time_dilation}
\tag{Time Dilation}
$$
Because this fraction is used in Relativity quite often, it is useful to define it as a separate function.
$$
\gamma = \frac{1}{\sqrt{1-\beta^2}} 
\label{eq:gamma}
\tag{Definition of Gamma}
$$
$v$ is always smaller than $c$ so;
$\gamma \geq 1$ [^3]

### Length Dilation
$$
l = v * \Delta t \label{eq:l0}
$$
$$
l' = v * \Delta t' \label{eq:l1}
$$
and using $\ref{eq:gamma}$;
$$
\Delta t' = \gamma * \Delta t
$$
when we apply this to $\ref{eq:l0}$ and $\ref{eq:l1}$:

$$
l = \frac{l_{0}}{\gamma} \leq l_{0}
\label{eq:length_dilation}
\tag{Length Dilation}
$$

* $l_{0}$ : Length observed by the stationary frame's observer.
* $v$ : Speed of the moving frame.
* $l$ : Length observed by the moving frame's observer.

### Lorentz Transformation Formulas
#### Transformation on Single Axis

* $x, y, z, t$ : Relative to the frame stationary to observer. 

* $x', y', z', t'$ : Relative to the moving frame.
$$
x' = \gamma (x-vt) \\
y' = y \\
z' = z \\
t' = \gamma(t - \frac{vx}{c^2})
\label{eq:lorentz_direct}
\tag{Lorentz Transformations}
$$

#### Reverse Transformation on Single Axis
$$
x = \gamma (x' + vt') \\
y = y' \\
z = z' \\
t = \gamma(t' + \frac{vx'}{c^{2}})
\label{eq:lorentz_reverse}
\tag{Reverse Lorentz Transformations}
$$

#### Speed Summation
Reference frame moving on x axis. 
$u'_{x}$ : Speed of moving object relative to the moving frame.
$u_{x}$ : Speed of moving object relative to the stationary frame.
$v$ : Speed of the moving frame.
$$
u_{x}' = \frac{u_{x} - v}{1 - \frac{1-vu_{x}}{c^{2}}} \label{eq:lorentz_speed_addition}
$$

### Doppler Event

$$
\lambda_o = \lambda s * \sqrt{\frac{1+\beta}{1-\beta}} \\
\beta : \ref{eq:relativity_beta}
$$

### Light Spectrum

![EMSpectrumcolor](C:\Users\anilh\Desktop\Modern Fizik\homeworks\docs\img\EMSpectrumcolor.jpg)

* $\lambda_o > \lambda_s$ : ***Redshift***, $v > 0$, Going away
* $\lambda_o < \lambda_s$ : ***Blueshift***, $v < 0$, Getting closer

### Space-Time Interval

$$
(\Delta S)^2 = (\Delta ct)^2 - (\Delta x)^2 \\
(\Delta S)^2 = (\Delta ct)^2 - (\Delta x)^2 - (\Delta y)^2 - (\Delta z)^2
$$

$$
(\Delta S)^2 = (ct_{2}-ct_{1})^2 - (x_{2}-x_{1})^2
\label{eq:space-time_interval}
$$

The spacetime interval is invariant.[^4]

| (in)equality       | interval type | region(s)       |
| ------------------ | ------------- | --------------- |
| $(\Delta S)^2 > 0$ | timelike      | past and future |
| $(\Delta S)^2 = 0$ | lightlike     | light cone      |
| $(\Delta S)^2 < 0$ | spacelike     | elsewhen        |
Space-Time Interval Table [^4]



### Examples

##### 1. Example 
In a bicycle race a cyclist has been shown a sign which states, leader of the race is at 15 seconds front with 4 m/s speed. Cyclist reads 3 m/s on his own speed gauge.[^1]

* **A)** What is the speed of the cyclist compared to the leader ?
* **B)** If we assume until the end of the race speeds of both bikers did not change. What is the distance between these two bikers ?

###### Solution
(With Newtonian mechanics)
*Input:* $v_{c}= 3 m/s$, $v_{l}= 4 m/s$, $\delta T = 15s$

1. Find the relative speed between the leader and the cyclist.
    $$
    \delta v = v_{l} - v_{c}
    \nonumber
    $$
    $$
    \delta v = 4 m/s - 3m/s \\
    \delta v = 1 m/s 
    \nonumber
    $$
1. Find distance between leader and the cyclist relative to cyclist at T=0 (Sign is shown).
    $$
    d_{cl} = v_{c} * \delta T
    \nonumber
    $$
    $$
    d_{cl} = 3 m/s * 15 s \\
    d_{cl} = 45m 
    \nonumber
    $$

##### 2. Example 
$R_{1}$ and $R_{2}$ rockets have idle length of $l_{10}$, $l_{20}$. They approach each other directly on a line. $R_{1}$'s observed length from earth is 60% of it's original length.[^2]

* **A)** Find velocity of $R_{1}$ observed from earth.

* **B)** $R_{2}$ approaches earth with velocity of 0.6c. Find velocity of $R_{2}$ observed from $R_{1}$.

###### Solution
Input: $l_{1} = l_{10} * 0.6 \nonumber$, $v_{R_{2}} = -0.6c$ (minus due direction)

**A)**
$$
L_{1} = \frac{L_{10}}{\gamma} = 0.6 * L_{10} \\
\gamma = \frac{1}{0.6} = \frac{1}{\sqrt{1-\beta^2}} \\
1 - \beta^2 = (\frac{3}{5})^2 \\
\beta = \frac{v}{c} \\
v = \frac{4}{5} c
\nonumber
$$

**B)** Since  $v_{R_{2}}$ is relative to earth frame S; take $R_{1}$ as the moving frame S'. 

S' has velocity $v=\frac{4}{5}c$

Using $\ref{eq:lorentz_speed_addition}$ and $R_{2}$'s velocity $v_{R_{2}} = -0.6c$ as $u_{x}$:
$$
u_{x}' = \frac{-0.6c - 0.8}{1 - \frac{1-0.8c * -0.6c}{c^{2}}}  \\
u_{x}'= \frac{(-0.6 * -0.8)c}{1+0.48} \\
u_{x}'= -\frac{1.4c}{1.48}
\nonumber
$$

##### 3. Example 
In S lab frame there is an explosion at $t = 4s$ moment.[^2]

**A)** S' reference frame has a speed relative to S frame of 0.6c. For aforementioned explosion to happen at the $(x' = y' = z'= 0)$ find the coordinates $(x, y, z)$ using Lorentz transformations.
	
Input : $v = 0.6c$ (Speed of the moving frame.)
Using $\ref{eq:gamma}$:
$$
\gamma = \frac{1}{\sqrt{1 - \frac{v^2}{c^2}}} \\
\gamma = \frac{1}{\sqrt{1 - \frac{0.6c^2}{c^2}}} \\
\gamma = \frac{5}{4}
\nonumber
$$
Using $\ref{eq:lorentz_direct}$:
$$
x' = \gamma (x-vt) = 0 \\
\frac{5 * (x-vt)}{4}  = 0 \\
(x-vt) = 0 \\
x = vt \\
x = 0.6c * 4s \\
x = 2.4 cs \\
\nonumber
$$

**B)** Find the time coordinate $t'$ for S' frame.
Using $\ref{eq:lorentz_direct}$, and found $\gamma$ values:
$$
t' = \gamma(t - \frac{vx}{c^2}) \\
t' = \frac{5}{4} (4s - \frac{0.6c * 2.4}{c^2} \\
t' = 3.20s
\nonumber
$$
t = 3.2 seconds for the observer in S' frame.

##### 4. Example
A space ship travels towards a star with velocity $v=0.6c$ [^2]

**A)** If the traveled distance measured by the space ship is 16 lightyears. Find the length of traveled distance observed from earth.

Input : $l=16c*years$, $v=0.6c$

Using $\ref{eq:gamma}$:
$$
\gamma = \frac{1}{ \sqrt{1 - (\frac{v}{c})^2}} \\
\gamma = \frac{1}{ \sqrt{1 - (\frac{0.6c}{c})^2}} \\
\gamma = \frac{5}{4}
\nonumber
$$
Using $\ref{eq:length_dilation}$
$$
l = \frac{l_{0}}{\gamma} \leq l_{0} \\
16c*years = \frac{l_{0}}{\frac{5}{4}} \\
l_{0} = 20 c*years
\nonumber
$$

**B)** Find the duration of the travel experienced in the space ship.
$$
t_{s} = \frac{l}{v_{s}} \\
t_{s} = \frac{16c*years}{0.6c} \\
t_{s} = \frac{80}{3} years \\
\nonumber
$$

**C)** Find the duration of the travel experienced on Earth.

Using the found $l_{0}$ value:
$$
t_{s} = \frac{l}{v_{s}} \\
t_{s} = \frac{20c*years}{0.6c} \\
t_{s} = \frac{100}{3} years \\
\nonumber
$$

or using $\ref{eq:time_dilation}$ and found $t_{s}$ value:
$$
t = \gamma * t' \\
t_{earth} = \gamma * t_{s} \\
t_{earth} = \frac{5}{4} *  \frac{80}{3} years \\
t_{earth} = \frac{100}{3} years 
\nonumber
$$

##### 5. Example
An observer in S frame observes 2 simultaneous lightning hits. One hit happens at $x_{1}=y_{1}=z_{1}=t_{1} = 0$ while other hit happens at $x_{2} = 900, y_{2}=z_{2}=t_{2} = 0$. [^2]

**A)** Find the coordinates of the events; observed by a stationary observer on moving S' frame. S' frame has velocity of 0.8c .

Using $\ref{eq:gamma}$:
$$
\gamma = \frac{1}{
\sqrt{1-(\frac{8}{10})^2}
} \\
\gamma = \frac{5}{3}
\nonumber
$$
Using $\ref{eq:lorentz_direct}$:

* First hit.

$$
x_{1}' = \gamma(x_{1}- vt_{1}) \\
x_{1}' = \frac{5}{3} (0 - 0.8 * 0) = 0 \
\nonumber
$$

$$
y_{1}' = y_{1} = 0 \\
z_{1}' = z_{1} = 0
\nonumber
$$

$$
t_{1}' = \gamma(t_{1}-\frac{vx_{1}}{c^2}) \\
t_{1}' = \frac{5}{3}(0-\frac{0.8c * 0}{c^2}) \\
t_{1}' = 0 \nonumber
$$

* Second hit.
  $$
  x_{2}' = \gamma(x_{2}- vt_{2}) \\
  x_{2}' = \frac{5}{3} (900m - 0.8 * 0) = \frac{5}{3} * 900m = 1500m \
  \nonumber
  $$

  $$
  y_{2}' = y_{2} = 0 \\
  z_{2}' = z_{2} = 0
  \nonumber
  $$

  $$
  t_{2}' = \gamma(t_{2}-\frac{vx_{2}}{c^2}) \\
  t_{2}' = \frac{5}{3}(0-\frac{0.8c * 900m}{c^2}) \\
  t_{2}' = \frac{5}{3}*-(0.8 * 900)mc^-1 \\
  t_{2}' = -1200m \nonumber
  $$

  

## Relative Mechanics

### Relative Momentum

Using $\ref{eq:gamma}$.
$$
\vec{p} = \frac{m\vec{u}}{\sqrt{1-\frac {u^2} {c^2}}} = \gamma m \vec{u}
\label{eq:relative_momentum}
\tag{Relative Momentum}
$$


##### Example

A 1kg piece of metal moves with 0.4c velocity. Found it's momentum.

###### Solution

Find gamma using $\ref{eq:gamma}$:
$$
\gamma = \frac {1}{\sqrt{1-(\frac{u}{c})^2}} \\
\gamma = \frac {1}{\sqrt{1-(\frac{0.4c}{c})^2}} \\
\gamma = 1.091
\nonumber
$$
Find momentum using $\ref{eq:relative_momentum}$:
$$
p = \gamma m u \\
p = 1.091 * 1 {kg} * 0.4{c} \\
p = 0.4364c * kg \\
p = 1.3083 * 10^8 m*kg*s^-1 \\
\nonumber
$$

### Relative Energy

$$
E=\frac{mc^2}{\sqrt{1-u^2/c^2}}=\gamma m c^2 \\
\label{eq:relative_energy}
\tag{Relative Energy}
$$

Using binomial approximation:
$$
E \approx mc^2+\frac{1}{2}mu^2 \\
\label{eq:relative_energy_approximation}
$$
Terms analyzed separately:
$$
mc^2\\
\tag{Rest Energy}
\label{eq:rest_energy}
$$

$$
\frac{1}{2}mu^2
\tag{Kinetic Energy}
$$

Using $\ref{eq:relative_energy}$ and $\ref{eq:relative_energy_approximation}$:
$$
E = mc^2 + K = \gamma m c^2\\
$$

$$
K = E-mc^2
$$

Combine:
$$
K = (\gamma m c^2) - mc^2 \\
K= (\gamma - 1) m c^2
$$

### Two Useful Correlations

Using definitions for $\ref{eq:relative_momentum}$ and $\ref{eq:relative_energy}$ we can write $\vec{\beta}$ dimensionless vector:
$$
\vec{\beta} \equiv \frac{\vec{u}}{c} = \frac {\vec{p}c} {E}
\label{eq:relative_beta_vector}
$$

And:
$$
E^2 = (pc)^2 + (mc^2)^2 \label{eq:relative_energy_2}
$$

### Units

* Relative speeds are generally written using $\ref{eq:relative_beta_vector}$; $\beta = u/c$.
* For energy generally eV is used. 
  * $$1eV = q \Delta V = (-1.6* {10^-19} C) * -1V = 1.6 * 10^19 J$$
* Mass can be used in $mc^2$ form with eV unit.

##### Example
Since electron's mass is $m=9.11 * 10^-31$ find it's $\ref{eq:rest_energy}$ as eV.

### Mass - Energy Transformation

Check examples in [^3] Page: 54-55

## Atoms

Introduction of this section is mostly Chemistry 101.

### Millikan Oil  Drop Experiment

Acceleration of an electron in an Electric and Magnetic field:
$$
m \vec{a} = -e(\vec{E}+\vec{u} \times \vec{B})
$$
Electric field required to suspense a drop in mid-air:
$$
qE = Mg
$$

### Bohr Quantization

$$
2\pi r = n\lambda
$$



### Bohr Atom Model

$$
U = - \frac{ke^2}{r}
$$

$$
K=-\frac12U
$$

$$
E=K+U = \frac12 U = - \frac12 \frac{ke^2}{r}
$$

### Hydrogen-Alike

$$
\Delta E = -Z^2 \frac{E_R}{n^2}
$$



### Moseley's Law

$$
\Delta E = 2.18 \times 10^-18 J (Z-1)^2 (\frac{1}{n_{1}^2}-\frac{1}{n_{2}^2})
$$

$$
\Delta E = E_{R}(Z-1)^2 (\frac{1}{n_{1}^2}-\frac{1}{n_{2}^2})
$$

For $K_{\alpha}$ (X-Ray) Emissions, $n_{1} = 1$, $n_{2} = 2$.

## Quantization of Light

### X-Ray Tubes

$$
K_{max} = V_se
$$

$$
\phi = hf_0 \\
f_o \text{: Limit frequency}
$$

$$
K_{max} = hf - \phi
$$



### Bragg's Law
$$
2d\sin\theta = n\lambda
$$

### Double Slit

$$
d\sin\theta = n\lambda \\
\lambda_{photon} \leq d
$$

### Compton Event

Using $\ref{eq:relative_energy_2}$:
$$
\text{for photons}\\
E = pc
$$

$$
\Delta \lambda = \lambda - \lambda_0 = \frac{h}{mc}(1-cos\theta) \\
\theta\text{: refraction angle}
$$

## Matter Waves





### de Broglie Hypothesis

$$
v = \lambda f \\
\Delta E = hf \\
\Delta E = hw \\
p = \frac{h}{\lambda}
$$

$$
\hslash = \frac{h}{2\pi} \\
$$

$$
E_{k} = \frac{p^2}{2m}
$$


$$
L=\frac{nh}{2\pi} \\ 
L = n \hslash
$$
L: Angular Momentum

## One Dimensional Schrodinger Equation

### One Dimensional Well

$$
E_{n} = n^2\frac{\pi^2\hslash^2}{2mL^2} \qquad (n = 1,2,3) \\
\qquad \\
\text{L: max range.}
$$

$$
\psi_{n}(x) = \sqrt{\frac2L}sin(\frac{n \pi x}{L})
$$


$$
\int_0^L|\Psi(x)|^2dx=1
$$
Examples at [^6]

## Three Dimensional Schrodinger Equation

## Sources
[^1]: Spring 2019, Homeworks, FIZ2132, YTU
[^2]: Previous Final Exams, Modern Physics, YTU
[^3]: Fen ve Mühendislikte Modern Fizik. John R Taylor; Chris D Zafiratos; Michael Andrew Dubson; Bekir Karaoğlu. 2nd Edition, ISBN: 978-605-5884-06-2
[^4]: <https://physics.info/space-time/> "Space-Time", Physics Hypertextbook. Accessed: 21st of May 2019.
[^5]: <https://www.youtube.com/watch?v=toGH5BdgRZ4> "Special Relativity | Lecture 1, Stanford"
[^6]: <http://www.nyu.edu/classes/tuckerman/adv.chem/lectures/lecture_6/node2.html>