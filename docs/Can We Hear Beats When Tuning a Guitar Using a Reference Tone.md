*Description and Information about Beats will be written later on.*


Let's try standard guitar tuning (E, B, G, D, A, E) 6th string's note is E4. 


E4's frequency is 329.63Hz. We can create this tone using Multi Oscillator Tone Generator software.

[![img](https://4.bp.blogspot.com/-SfZ98H39AIg/W9h91AurjJI/AAAAAAAAIC0/aMc1cgiWyuMkQZV9AT_nMR4VO3R7s9vuQCLcBGAs/s400/Ekran%2BAl%25C4%25B1nt%25C4%25B1s%25C4%25B12.PNG)](https://4.bp.blogspot.com/-SfZ98H39AIg/W9h91AurjJI/AAAAAAAAIC0/aMc1cgiWyuMkQZV9AT_nMR4VO3R7s9vuQCLcBGAs/s1600/Ekran%2BAl%C4%B1nt%C4%B1s%C4%B12.PNG)

Now let's try generating a tone which has slightly higher frequency. In this case 332.63Hz, only 3Hz higher than E4.

[![img](https://2.bp.blogspot.com/-eC-6HFailm8/W9h9RRActZI/AAAAAAAAICw/vhdhYS3gt5UV-YVQnlSwzPDVPOkC-ylxgCEwYBhgL/s400/Ekran%2BAl%25C4%25B1nt%25C4%25B1s%25C4%25B1.PNG)](https://2.bp.blogspot.com/-eC-6HFailm8/W9h9RRActZI/AAAAAAAAICw/vhdhYS3gt5UV-YVQnlSwzPDVPOkC-ylxgCEwYBhgL/s1600/Ekran%2BAl%C4%B1nt%C4%B1s%C4%B1.PNG)

Using Audacity we record both tones in different tracks. Giving a slight offset to second track will allow us to hear the tones separately at start and end.

[![img](https://4.bp.blogspot.com/-Ji5NcwSIADU/W9h-_RlQ7YI/AAAAAAAAIDA/itIb0k3B5OsUoBs2NPexKqqVxp55_OfXQCLcBGAs/s640/Ekran%2BAl%25C4%25B1nt%25C4%25B1s%25C4%25B13.PNG)](https://4.bp.blogspot.com/-Ji5NcwSIADU/W9h-_RlQ7YI/AAAAAAAAIDA/itIb0k3B5OsUoBs2NPexKqqVxp55_OfXQCLcBGAs/s1600/Ekran%2BAl%C4%B1nt%C4%B1s%C4%B13.PNG)

 Now the moment of truth, let's go ahead and play the project. 

Eureka ! First, we are hearing first tone then both tones with clearly noticeable periodic fluctuation in intensity and then the second tone.

Let's export the project as a wav sound file in order to observe this fluctuation, the beats.  

[![img](https://4.bp.blogspot.com/-BhHIuHHmLh8/W9iAVBAPcSI/AAAAAAAAIDM/YQVaS7sRUOsq2pbMgRBD9dWO9sL_5rOdwCLcBGAs/s1600/Ekran%2BAl%25C4%25B1nt%25C4%25B1s%25C4%25B14.PNG)](https://4.bp.blogspot.com/-BhHIuHHmLh8/W9iAVBAPcSI/AAAAAAAAIDM/YQVaS7sRUOsq2pbMgRBD9dWO9sL_5rOdwCLcBGAs/s1600/Ekran%2BAl%C4%B1nt%C4%B1s%C4%B14.PNG)

In waveform we can clearly see the fluctuations.

[![img](https://3.bp.blogspot.com/-VERKeok2HhA/W9iBJ8xCgmI/AAAAAAAAIDY/irPFMSiL-tA8LUtJhb4gX7pezcPOEQISQCEwYBhgL/s1600/Ekran%2BAl%25C4%25B1nt%25C4%25B1s%25C4%25B15.PNG)](https://3.bp.blogspot.com/-VERKeok2HhA/W9iBJ8xCgmI/AAAAAAAAIDY/irPFMSiL-tA8LUtJhb4gX7pezcPOEQISQCEwYBhgL/s1600/Ekran%2BAl%C4%B1nt%C4%B1s%C4%B15.PNG)

  
Let's roughly measure the period of the fluctuations using selection tool.

11.142 - 10.810 =  0.332 seconds. 
So peak to peak frequency is 1/0.332 = 3Hz. 

This means this fluctuations are happening 3 times a second. Quite noticeable for human ears. But do not take my word for it. Listen it yourself ! 

<iframe width="560" height="315" src="https://www.youtube.com/embed/7f1fKqGgyiw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>