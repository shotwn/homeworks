# Planck Sabitinin Deneysel Olarak Tespit Edilmesi
## Planck Sabiti
Planck sabiti Max Planck tarafından 1900 yılında tanımlanmıştır.

19\. Yüzyılın sonlarında termodinamik denge konumundaki bir cismin yaydığı termal elektromanyetik radyasyon ile bu cismin sıcaklığı arasında bir ilişki olduğu yani **siyah cisim ışıması** biliniyordu.

### Reyleigh-Jeans
Lord Rayleigh ve James Jeans bu ışımayı açıklamaya klasik fizik yöntemleri ile yaklaştılar. Bu açıklama yüksek dalga boylarında $(10^5 GHz)$ deneysel verilere yakınlık göstermişti fakat düşük dalga boylarında deneysel verilerden uzaklaşıyor ve enerji seviyesinin sonsuza gideceğini ön görüyordu. Açıklamadaki bu eksiklik daha sonra *morötesi felaketi* olarak anılmaya başlandı.

###### Reyleigh-Jeans Formülü

$$
B_{\lambda}(T)=\frac{2ck_{B}T}{\lambda^4}
$$

- $k_{B}$ : Boltzman sabiti
- T : Sıcaklık
- $\lambda$ : Dalga boyu
- c : Işık Hızı

### Planck Yasası

Max Planck siyah cisim ışımasını düşük frekanslarda matematiksel olarak açıklayabilmek için ışımaların belirli bir enerjiye sahip ayrık paketlerin (kuanta) katları halinde olduğunu varsaydı.  Bu, döneminde hakim olan, "her değer sonsuz sayıda parçalardan oluşur"[^6][^7] anlayışına ters bir yaklaşımdı. Planck bulduğu çözümün matematiksel olarak anlamlı fakat fiziksel olarak doğru olmadığını düşünmüştü.[^2][^5]

Fakat 1905 yılında Albert Einstein ve Satyendra Nanh Bose, Planck sabitinin doğada bulunan bir değer olduğunu öngördü. 1914 yılında ise Millikan'ın yağ damlası deneyi ile bu teori kanıtlanmış oldu.[^1]

Bugün Planck yasasının ön gördüğü enerji paketlerini *foton*, fotonların yaptığı ışıma olayını da *fotoelektrik olay* olarak tanımlıyoruz.

###### Planck Yasası Formülü

$$
B_{\lambda}(\lambda,T)=\frac{2hc^2}{\lambda^5}\frac{1}{\frac{hc}{e^{\lambda k_{B} T}}-1}
$$

- h : Planck sabiti

## Planck Sabitinin Ölçülmesi

### LED (Light Emitting Diot)
P-N yarı iletken çiftinden yapılmış bir ışık kaynağıdır. P-N çifti ileri yönlü gerilim altındayken çiftin arasındaki bariyer bölge genişliği azalır. Belirli bir gerilim sabiti geçildiğinde ise bariyer bölge çok incelir ve P-N çifti iletim yapmaya başlar. Bu iletim sırasında elektron-hol taşıyıcıları arasındaki geçişlerde, enerji seviyesi değişen elektronlar elektromanyetik ışıma yapar. Işıma olayını başlatmak için gereken gerilim miktarına eşik gerilimi adı verilir.

İleri yönlü gerilim esnasında oluşan ışığın enerji / dalgaboyu ilişkisi
$$
E=\frac{hc}{\lambda} \label{exp1}
$$
ile tanımlanır. Burada:
- c : Işık hızı.
- h : Planck sabiti.
- $\lambda$ ise ışığın dalga boyudur.



Eşik gerilimine ulaşıldığı anda bariyer bölgeyi geçen elektronlara verilen enerji miktarı ise
$$
E=eV \label{exp2}
$$
ile tanımlanır. $\ref{exp1}$ ve $\ref{exp2}$ ile:
$$
eV=\frac{hc}{\lambda}
$$
elde edebiliriz. Burada sabit değerleri,  $1/\lambda$ değerine çarpan olarak tanımlarsak:
$$
V=\frac{hc}{e} \frac{1}{\lambda}
$$
$$
h=\frac{e}{c} \lambda V \label{exp3}
$$
formülleri bize
$$
s = \frac{hc}{e}
$$
s değerinin $V$, $\frac{1}{\lambda}$ grafiğinin eğimine eşit olduğunu gösterir. Buradan Planck sabitini çekersek:
$$
h=\frac{e}{c}s \label{exp4}
$$
formülünü elde ederiz.[^3]



$\lambda$ değerinin deneysel yöntemlerde çok dağılım göstermemesi için kullanılacak LED'in olabildiğince monokromatik; tek dalga boyunda ışık üreten bir çeşit olması gereklidir.

## Metod

![Circuit](img/circuit-20190514-1815.jpg) 
[Interactive Circuit](http://tinyurl.com/y5x4kjfa)[^4]

### Ölçüm
|Renk|$\lambda$|V|h|
|---|---|---|---|
| - |  - | -  | -  |
Örnek Tablo

1. Renk, Dalga boyu($\lambda$), eşik gerilimi (V) ve h Planck sabiti sütunlarına sahip bir tablo hazırlanır.
1. Şekildeki devre değişken gerilim kaynağı ve monokromatik LED kullanılarak hazırlanır.
1. LED'in rengi ve rengin dalga boyu ($\lambda$) tabloya işlenir.
1. Gerilim kaynağının değeri LED ışık vermeye başlayana kadar yavaşça arttırılır.
1. LED'nin ışık vermeye başladığı andaki gerilim değeri Voltmetreden okunulur ve not alınır.
1. LED değiştirilerek, yeni bir LED ile ölçüm tekrarlanır.

### Hesaplama
1. Her ölçüm için $\ref{exp3}$ bağıntısı kullanılarak h Planck sabiti hesaplanır ve tabloya işlenir.
1. Bulunan h değerlerinin aritmetik ortalaması alınır.
1. Tablodaki ölçüm değerleri kullanılarak V, $1/\lambda$ grafiği çizilir.
1. Grafiğin eğimi alınır ve $\ref{exp4}$ bağıntısıyla Planck sabiti hesaplanır.

Aritmetik ortalama ile bulunan değer ve grafik ile bulunan değer, Planck sabitinin bilinen değeri olan $h=6.62607004 * 10^{-34} m^2 kg*s^-1$ ile karşılaştırılarak bağıl hata bulunur.

## Kaynaklar

<small>Erişim: 10-14 May 2019</small>

[^1]: <https://www.nobelprize.org/prizes/physics/1923/ceremony-speech/>	"Nobelprize.org"
[^2]: <https://physics.info/planck/>	"Physics Hypertextbook"
[^3]: <https://vlab.amrita.edu/?sub=1&brch=195&sim=547&cnt=1>	"Amrita Vishwa Vidyapeetham Research Academy Virtual Lab."
[^4]: <http://tinyurl.com/y5x4kjfa> "Interactive Cirtcuit F. Anıl Haksever, Falstad Electronics Simulator"

[^5]: <https://youtu.be/tQSbms5MDvY> "Planck's Constant and The Origin of Quantum Mechanics | Space Time | PBS Digital Studios"
[^6]: <https://www.iep.utm.edu/zeno-par/> "Zeno's Paradoxes"
[^7]: <http://philsci-archive.pitt.edu/1197/1/Zeno_s_Paradoxes_-_A_Timely_Solution.pdf> "Zeno's Paradoxes: A Timely Solution by Peter Lynds"